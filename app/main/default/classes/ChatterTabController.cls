public class ChatterTabController {
    @AuraEnabled
    public static list<Chatter__c> getComments(){
        List <Chatter__c> chtrlst = new List<Chatter__c>([SELECT Id,  IsDeleted, Name, CreatedDate, CreatedBy.Name, 
                                                           LastModifiedDate, LastModifiedById, Chatter_Sub_Comment__c,
                                                           Comments__c, SubComments__c, CreatedBy.SmallPhotoUrl,(Select Id,  IsDeleted, Name, CreatedDate, CreatedBy.Name, 
                                                           LastModifiedDate, LastModifiedById, Chatter_Sub_Comment__c,
                                                           Comments__c, SubComments__c, CreatedBy.SmallPhotoUrl FROM Chatters__r ORDER BY LastModifiedDate ASC )
                                                           FROM Chatter__c WHERE Chatter_Sub_Comment__c = null ORDER BY LastModifiedDate DESC]);

        system.debug(chtrlst);
        return chtrlst;
    }
    
     @AuraEnabled
    public static list<Chatter__c> getSubComments(){
        List <Chatter__c> chtrlst = new List<Chatter__c>([SELECT Id,  IsDeleted, Name, CreatedDate, CreatedBy.Name, 
                                                           LastModifiedDate, LastModifiedById, 
                                                           Comments__c, SubComments__c, CreatedBy.SmallPhotoUrl,
                                                          Chatter_Sub_Comment__r.SubComments__c
                                                           FROM Chatter__c ORDER BY LastModifiedDate DESC]);
        List <Chatter__c> chtrlsttmp = new List<Chatter__c>();
        for(Chatter__c c: chtrlst){
            if(c.Comments__c == null){
                
                chtrlsttmp.add(c);
            }
        }
        system.debug(chtrlsttmp);
        return chtrlsttmp;
    }
    @AuraEnabled 
    public static String save(String chatter){
        Chatter__c chat= new Chatter__c();
        chat.Comments__c = chatter;
        insert chat;
        return null; 
    }
    
    @AuraEnabled
    public static List<User> searchUsers(String usr){
        String tempUsr = '%'+usr+'%';
        return [SELECT Id, Username, LastName, FirstName, Name, SmallPhotoUrl  FROM User WHERE FirstName LIKE: tempUsr OR LastName LIKE: tempUsr];
        
    }
    @AuraEnabled
    public static String getUserId(){
        return userInfo.getUserId();
        
    }
    @AuraEnabled
    public static void addComment(String docid){
        ContentDocument cd =[SELECT Id,  CreatedBy.Name, Title FROM ContentDocument WHERE Id =:docid];
        
        Chatter__c chat= new Chatter__c();
        chat.Comments__c = '<p> A new file was added </p>'+'<p> File owner: <b>'+ cd.CreatedBy.Name+'</b></p>'+'<p> Filename: <b>'+cd.Title+'</b></p>';
        insert chat;
    }
    
    @AuraEnabled
    public static list<Chatter__c> getOnlyToMe(){
        list <Chatter__c> chtrlst = new List<Chatter__c>([SELECT Id,  IsDeleted, Name, CreatedDate, CreatedBy.Name, 
                                                          LastModifiedDate, LastModifiedById, Comments__c, 
                                                          CreatedBy.SmallPhotoUrl 
                                                          FROM Chatter__c ORDER BY LastModifiedDate DESC ]);
        String currentUsr = UserInfo.getUserId();
        User usr = new User();
        list <Chatter__c> tmpchtrlst = new List<Chatter__c>();
        usr = [SELECT Id, Name FROM User WHERE Id=:currentUsr];
        system.debug(usr.Name);
        
        for(Chatter__c c: chtrlst){            
            if(c.Comments__c != null && c.Comments__c.contains(usr.Name)){
                tmpchtrlst.add(c);
            }
        }
        system.debug(tmpchtrlst);
        return tmpchtrlst;
    }
    
    @AuraEnabled 
    public static String saveSubComment(String parentId, String commentMessage){
        system.debug(parentId);
        system.debug(commentMessage);
        Chatter__c c=new Chatter__c();
        c.Chatter_Sub_Comment__c =parentId;
        c.SubComments__c=commentMessage;
        insert c;
        return null;
    }
    @AuraEnabled 
    public static list<Chatter__c> getOpenedCases(){
        list <Chatter__c> chtrlst = new List<Chatter__c>([SELECT Id, Comments__c FROM Chatter__c ]);
      // list <Case>openCase = new list <Case>([SELECT Id, ContactId, AccountId, CaseNumber, Status, Type FROM Case WHERE not (Status like 'Closed') ]);
         list <Chatter__c> tmpchtrlst = new List<Chatter__c>();
        system.debug('aaaaaaaaaaaaaaa');
         for(Chatter__c c: chtrlst){
                  if(c.Comments__c != null && c.Comments__c.contains('Case') && !c.Comments__c.contains('Status: Closed')){
                system.debug('bbbbbbbbbbbbbbbbbbbb');
                tmpchtrlst.add(c);
            }
        }
        
        return tmpchtrlst;
    }
     @AuraEnabled 
    public static String loadFilterSearch(String picklist){
        system.debug(picklist);
        string ab;
        
        
        return null;
    }
    /*
     @AuraEnabled
    public static List<String> getPicklistvalues(String objectName, String field_apiname,Boolean nullRequired){
        List<String> optionlist = new List<String>();
        
        Map<String,Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
        Map<String, Schema.SObjectField> field_map = gd.get(objectName.toLowerCase()).getDescribe().fields.getMap(); 
        
        List<Schema.PicklistEntry> picklistValues = field_map.get(field_apiname).getDescribe().getPickListValues();
        
        if(nullRequired == true){
            optionlist.add('--None--');
        }
        
        for (Schema.PicklistEntry pv : picklistValues) {
            optionlist.add(pv.getValue());
        }
        return optionlist;
    }
    /*
@AuraEnabled

public static String saveSubComment(String chatter, String chatterId){
SubComment__c subc = new SubComment__c();
subc.Chatter__c = chatterId;
subc.SubComment__c = chatter;
if(chatter!=null || chatter!=''){
system.debug(subc);
insert subc;
}

SubComment__c tempsubc = [Select id, SubComment__c, Chatter__c FROM SubComment__c WHERE Chatter__c =:chatterId];
system.debug(tempsubc);
Chatter__c chat = [SELECT id, SubComment__c FROM Chatter__c Where id=:chatterId];
chat.SubComment__c=tempsubc.Id;
update chat;
system.debug(chat);

return null;
}*/
    /*
@AuraEnabled
public static list<SubComment__c> getSubComments(String chatid){
system.debug(chatid);

return [SELECT Id, IsDeleted, Name, CreatedDate, CreatedBy.Name, LastModifiedDate, LastModifiedById, Chatter__c,  SubComment__c, CreatedBy.SmallPhotoUrl 
FROM SubComment__c ORDER BY LastModifiedDate DESC ];
}*/
    
}