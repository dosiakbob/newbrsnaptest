<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>customamilAlert</fullName>
        <description>customamilAlert</description>
        <protected>false</protected>
        <recipients>
            <recipient>bohdan@dosiak-company.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>insightssecurity@00d1r000002dl78eac.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>integration@00d1r000002dl78eac.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rjain@flosum.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/MarketingProductInquiryResponse</template>
    </alerts>
</Workflow>
