({
    init: function (cmp, event, helper) {
        cmp.set('v.columns', [
            {label: 'Issue Type', fieldName: 'issueType', type: 'text'},
            {label: 'Key', fieldName: 'key', type: 'button', typeAttributes: { variant: 'base', label: 'button'}},
            {label: 'Priority', fieldName: 'priority', type: 'text'},
            {label: 'Summary', fieldName: 'summary', type: 'text'},
            {label: 'Description', fieldName: 'description', type: 'text'},
            {label: 'Operations', fieldName: 'operations', type: 'button', typeAttributes: { variant: 'base', label: 'Configure'}},
            {label: '', fieldName: 'operations', type: 'button', typeAttributes: { variant: 'base', label: 'Pull'}},
            {label: '', fieldName: 'operations', type: 'button', typeAttributes: { variant: 'base', label: 'Push'}},
            {label: '', fieldName: 'operations', type: 'button', typeAttributes: { variant: 'base', label: 'Unlink'}}
        ]);
        
        
            }
            })