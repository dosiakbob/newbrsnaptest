({
    doinitMethod: function (component, event, helper) {
        var action = component.get("c.getComments");
        var keyup = component.get("v.newComment");
        action.setCallback(this, function (response) {
            var state = response.getState();
            component.set("v.newComment", "");
            component.set("v.comments", response.getReturnValue());
        });
        $A.enqueueAction(action);
    },

    searchUsers: function (component, event, helper) {
        var action = component.get("c.searchUsers");
        var keyup = component.get('v.newComment');
        keyup = keyup.substring(keyup.lastIndexOf('@') + 1, keyup.indexOf('<', keyup.lastIndexOf('@')));
        if (keyup != null || keyup != '') {
            action.setParams({
                "usr": keyup
            });
        }
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var name = response.getReturnValue();
                var str = [];
                for (let user of name) {
                    str.push({ value: user.Id, label: user.Name, logo: user.SmallPhotoUrl })
                }
                component.set('v.toUsers', str);
            }
            else if (state === "ERROR") {
                alert("Failed");
            }
        });
        $A.enqueueAction(action);
    },
    getcurrentUserId: function (component) {
        var action = component.get("c.getUserId");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var name = response.getReturnValue();
                component.set('v.userId', name);
            }
            else if (state === "ERROR") {
                alert("Failed");
            }
        });
        $A.enqueueAction(action);

    },
    addCommentAfterUpload: function (component, event, helper) {
        var action = component.get("c.addComment");
        var getId = component.get('v.docId');
        action.setParams({
            "docid": getId
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {

            }
            else if (state === "ERROR") {
                alert("Failed");
            }
        });
        $A.enqueueAction(action);

    },

    showOnlyToMe: function (component, event, helper) {
        var action = component.get("c.getOnlyToMe");
        var keyup = component.get("v.newComment");
        action.setCallback(this, function (response) {
            var state = response.getState();
            component.set("v.newComment", "");
            component.set("v.comments", response.getReturnValue());
        });
        $A.enqueueAction(action);

    },
    showOpenedCases: function (component, event, helper) {
        var action = component.get("c.getOpenedCases");
        var keyup = component.get("v.newComment");
        action.setCallback(this, function (response) {
            var state = response.getState();
            component.set("v.newComment", "");
            component.set("v.comments", response.getReturnValue());
        });
        $A.enqueueAction(action);

    },

    /*
     doSubinitMethod : function(component, event, helper) {
        var action= component.get("c.getSubComments");
        var keyup = component.get("v.newSubComment");
        action.setCallback(this,function(response){
            var state= response.getState();
            $A.log(response);
            component.set("v.newSubComment","");
            component.set("v.comments",response.getReturnValue());
        });
        $A.enqueueAction(action);
    }, 
    */
    /*
       getNextPage : function( component ) {

        var pageNumber = component.get( 'v.pageNumber' );
        var pageSize = component.get( 'v.pageSize' );

        pageNumber++;

        component.set( 'v.pageNumber', pageNumber );
        component.set( 'v.pageSize', pageSize );

        this.firePageChangeEvent( component, pageNumber, pageSize );

    },
     firePageChangeEvent : function( component, pageNumber, pageSize ) {

        component.getEvent( 'pageChangeEvent' ).setParams({
            'pageNumber' : pageNumber,
            'pageSize' : pageSize
        }).fire();

    },
  */
})